# variables
variable "priv_key" {
  description = "private key location"
}
variable "zone_id" {
  description = "zone_id"
}
variable "ip_address" {
    description = "IP address of this forwarding rule."
    default     = ""
}

data "google_project" "project" {
}
locals {
  project_id = "${data.google_project.project.project_id }"
}
resource "google_project_service" "compute_service" {
  project = local.project_id
  service = "compute.googleapis.com"
}
resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = false
  delete_default_routes_on_create = true
  depends_on = [
    google_project_service.compute_service
  ]
}

#drives
resource "google_compute_disk" "disk0" {
  name = "disk1-53"
  size = 10
  zone = "us-central1-a"
}
resource "google_compute_disk" "disk1" {
  name = "disk2-53"
  size = 8
  zone = "us-central1-a"
}

# #staticip
# resource "google_compute_address" "static_ip" {
#   name = "less53address"
# }

#firewall
resource "google_compute_network" "default" {
  name = "test-network"
}

resource "google_compute_firewall" "compute_firewall_rule" {
  name    = "terraform-tcp-allow"
  network = google_compute_network.default.name
 
  dynamic "allow" {
    for_each = ["80", "8001","8802"]
    content {
      protocol                = "tcp"
      ports                   = [allow.value]
    }
  }
}

#image
data "google_compute_image" "debian_image" {
  family  = "debian-10"
  project = "debian-cloud"
}
#instance 1
resource "google_compute_instance" "node1" {
        name = "node1"
        machine_type = "e2-medium"
        network_interface {
          network = "default"
          access_config {
          }
        }
        boot_disk {
          initialize_params {
                  image = data.google_compute_image.debian_image.self_link
          }
        }
        attached_disk {
          source = "disk1-53"
        }

        connection {
          user = "mard3n"
          private_key = "${file(var.priv_key)}"
          timeout = "2m"
          host = "${self.network_interface.0.access_config.0.nat_ip}"
        }
          provisioner "remote-exec" {
            script = "script.sh"
          }
          provisioner "local-exec" {
            command = "echo ${self.name} ansible_host=${self.network_interface.0.access_config.0.nat_ip} >> hostfile"  
            }
          provisioner "local-exec" {
          command = "ansible-playbook -l ${self.name} -i hostfile project.yml"          
  }
}
#instance 2
resource "google_compute_instance" "node2" {
        name = "node2"
        machine_type = "e2-medium"
        network_interface {
          network = "default"
          access_config {
          #   nat_ip = google_compute_address.static_ip.address
          }
        }
        boot_disk {
          initialize_params {
                  image = data.google_compute_image.debian_image.self_link
          }
        }
        # attached_disk {
        #   source = "disk1-53"
        # }
        attached_disk {
          source = "disk2-53"
        }
        connection {
          user = "mard3n"
          private_key = "${file(var.priv_key)}"
          timeout = "2m"
          host = "${self.network_interface.0.access_config.0.nat_ip}"
        }
        provisioner "remote-exec" {
          script = "script.sh"
        }
        provisioner "local-exec" {
            command = "echo ${self.name} ansible_host=${self.network_interface.0.access_config.0.nat_ip} >> hostfile"
  }
        provisioner "local-exec" {
          command = "ansible-playbook -l ${self.name} -i hostfile project.yml"          
        }
}

resource "google_compute_instance_group" "webservers" {
  name        = "terraform-webservers"
  description = "Terraform test instance group"
  instances = [
    google_compute_instance.node1.self_link,
    google_compute_instance.node2.self_link
  ]

  named_port {
    name = "http"
    port = "80"
  }
}
resource "google_compute_health_check" "webservers-health-check" {
  name        = "webservers-health-check"
  description = "Health check via tcp"

  timeout_sec         = 5
  check_interval_sec  = 10
  healthy_threshold   = 3
  unhealthy_threshold = 2

  tcp_health_check {
    port_name          = "http"
  }

  depends_on = [
    google_project_service.compute_service
  ]
}

# Global backend service
resource "google_compute_backend_service" "webservers-backend-service" {

  name                            = "webservers-backend-service"
  timeout_sec                     = 30
  connection_draining_timeout_sec = 10
  load_balancing_scheme = "EXTERNAL"
  protocol = "HTTP"
  port_name = "http"
  health_checks = [google_compute_health_check.webservers-health-check.self_link]

  backend {
    group = google_compute_instance_group.webservers.self_link
    balancing_mode = "UTILIZATION"
  }
}

resource "google_compute_url_map" "default" {

  name            = "website-map"
  default_service = google_compute_backend_service.webservers-backend-service.self_link
}

# Global http proxy
resource "google_compute_target_http_proxy" "default" {

  name    = "website-proxy"
  url_map = google_compute_url_map.default.id
}

# Regional forwarding rule
resource "google_compute_forwarding_rule" "webservers-loadbalancer" {
  name                  = "website-forwarding-rule"
  ip_protocol           = "TCP"
  ip_address            = "${var.ip_address}"
  port_range            = 80
  load_balancing_scheme = "EXTERNAL"
  network_tier          = "STANDARD"
  target                = google_compute_target_http_proxy.default.id
}

resource "google_compute_firewall" "load_balancer_inbound" {
  name    = "nginx-load-balancer"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  direction = "INGRESS"
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["nginx-instance"]
}

# resource "cloudflare_record" "juneway" {
#         name = "marden.juneway.pro"
#         value = google_compute_forwarding_rule.webservers-loadbalancer.ip_address
#         type = "A"
#         ttl = 3600
#         zone_id = var.zone_id
# }